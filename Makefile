EXE = taxi
OBJS = main.o taxi_window.o File.o

OBJS += lib/imgui_impl_sdl_gl3.o lib/imgui.o lib/imgui_demo.o lib/imgui_draw.o  
OBJS += lib/gl3w/GL/gl3w.o

LIBS = -lGL -ldl `sdl2-config --libs`

CXXFLAGS = -Ilib/ -Ilib/gl3w `sdl2-config --cflags`
CXXFLAGS += -std=c++1z -lstdc++fs -Wall -Wformat 
CFLAGS = $(CXXFLAGS) 

# CXX = clang
CXX = g++-7

.cpp.o:
	$(CXX) $(CXXFLAGS) -c -o $@ $<

all: $(EXE)
	@echo Build complete.

$(EXE): $(OBJS)
	$(CXX) -o $(EXE) $(OBJS) $(CXXFLAGS) $(LIBS)

clean:
	rm $(EXE) $(OBJS)
