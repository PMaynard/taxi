
#if defined(_MSC_VER) && !defined(_CRT_SECURE_NO_WARNINGS)
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "imgui.h"
#include <ctype.h>          // toupper, isprint
#include <math.h>           // sqrtf, powf, cosf, sinf, floorf, ceilf
#include <stdio.h>          // vsnprintf, sscanf, printf
#include <stdlib.h>         // NULL, malloc, free, atoi
#if defined(_MSC_VER) && _MSC_VER <= 1500 // MSVC 2008 or earlier
#include <stddef.h>         // intptr_t
#else
#include <stdint.h>         // intptr_t
#endif

#ifdef _MSC_VER
#pragma warning (disable: 4996) // 'This function or variable may be unsafe': strcpy, strdup, sprintf, vsnprintf, sscanf, fopen
#define snprintf _snprintf
#endif
#ifdef __clang__
#pragma clang diagnostic ignored "-Wold-style-cast"             // warning : use of old-style cast                              // yes, they are more terse.
#pragma clang diagnostic ignored "-Wdeprecated-declarations"    // warning : 'xx' is deprecated: The POSIX name for this item.. // for strdup used in demo code (so user can copy & paste the code)
#pragma clang diagnostic ignored "-Wint-to-void-pointer-cast"   // warning : cast to 'void *' from smaller integer type 'int'
#pragma clang diagnostic ignored "-Wformat-security"            // warning : warning: format string is not a string literal
#pragma clang diagnostic ignored "-Wexit-time-destructors"      // warning : declaration requires an exit-time destructor       // exit-time destruction order is undefined. if MemFree() leads to users code that has been disabled before exit it might cause problems. ImGui coding style welcomes static/globals.
#if __has_warning("-Wreserved-id-macro")
#pragma clang diagnostic ignored "-Wreserved-id-macro"          // warning : macro name is a reserved identifier                //
#endif
#elif defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"          // warning: cast to pointer from integer of different size
#pragma GCC diagnostic ignored "-Wformat-security"              // warning : format string is not a string literal (potentially insecure)
#pragma GCC diagnostic ignored "-Wdouble-promotion"             // warning: implicit conversion from 'float' to 'double' when passing argument to function
#pragma GCC diagnostic ignored "-Wconversion"                   // warning: conversion to 'xxxx' from 'xxxx' may alter its value
#if (__GNUC__ >= 6)
#pragma GCC diagnostic ignored "-Wmisleading-indentation"       // warning: this 'if' clause does not guard this statement      // GCC 6.0+ only. See #883 on github.
#endif
#endif

// Play it nice with Windows users. Notepad in 2015 still doesn't display text data with Unix-style \n.
#ifdef _WIN32
#define IM_NEWLINE "\r\n"
#else
#define IM_NEWLINE "\n"
#endif

#define IM_ARRAYSIZE(_ARR)  ((int)(sizeof(_ARR)/sizeof(*_ARR)))
#define IM_MAX(_A,_B)       (((_A) >= (_B)) ? (_A) : (_B))

///------------
#include <iostream>
#include <map>
#include <string>
#include <typeinfo>


#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

#define INPUT_MAX 128

#include "File.h"

void MainWindow(bool* p_open)
{
    static bool show_main_window = true;
    static bool menu_quit = false;

    static bool reload_pwd = false;

    if(menu_quit) ImGui::End();

    static bool no_titlebar = true;
    static bool no_border = true;
    static bool no_resize = false;
    static bool no_move = true;
    static bool no_scrollbar = false;
    static bool no_collapse = true;
    static bool no_menu = false;

    ImGuiWindowFlags window_flags = 0;
    if (no_titlebar)  window_flags |= ImGuiWindowFlags_NoTitleBar;
    if (!no_border)   window_flags |= ImGuiWindowFlags_ShowBorders;
    if (no_resize)    window_flags |= ImGuiWindowFlags_NoResize;
    if (no_move)      window_flags |= ImGuiWindowFlags_NoMove;
    if (no_scrollbar) window_flags |= ImGuiWindowFlags_NoScrollbar;
    if (no_collapse)  window_flags |= ImGuiWindowFlags_NoCollapse;
    if (!no_menu)     window_flags |= ImGuiWindowFlags_MenuBar;
    ImGui::SetNextWindowPos(ImVec2(0,0));
    ImGui::SetNextWindowSize(ImVec2(550,680), ImGuiCond_FirstUseEver);
    if (!ImGui::Begin("Main Window", &show_main_window, window_flags))
    {
        ImGui::End();
        return;
    }

    // Menu Bar
    if (ImGui::BeginMenuBar())
    {
        if (ImGui::BeginMenu("File"))
        {
            if (ImGui::MenuItem("Close")) *p_open = false;
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    } 

    std::string current = fs::current_path(); 

    static char pwd[INPUT_MAX];
    strcpy(pwd, current.c_str());

    ImGui::InputText(":PWD", pwd, INPUT_MAX);
    static std::map <std::string, File> file_cache;

    ImGuiIO& io = ImGui::GetIO();
    ImGui::Text("Keys down:");
    for (int i = 0; i < IM_ARRAYSIZE(io.KeysDown); i++)
    {
        if (io.KeysDownDuration[i] >= 0.0f && i == 13)
        {
            ImGui::SameLine();
            ImGui::Text("%d (%.02f secs)", i, io.KeysDownDuration[i]);
            reload_pwd = true;
        }
    }

    if(reload_pwd){
        try{
            for(auto& p: fs::directory_iterator(pwd)){
                File temp = File(p.path());
                file_cache[p.path().c_str()] = temp;
            }

        } catch(fs::filesystem_error& e) {
            std::cout << e.what() << '\n';
        }
        reload_pwd = false;
    }

    ImGui::Columns(2, "mycolumns");
    ImGui::Separator();
    ImGui::Text("Path"); ImGui::NextColumn();
    ImGui::Text("Size"); ImGui::NextColumn();
    ImGui::Separator();

    for( auto const& [key, val] : file_cache )
    {
        // std::cerr << key << "\n";
        ImGui::Text(val.get_filename());
        ImGui::NextColumn();
        if(fs::is_directory(val.get_path())){
            ImGui::Text("Directory"); 
        }else{
            ImGui::Text("%f", val.get_size()); 
        }
        ImGui::NextColumn();
    }

    ImGui::End();
}
