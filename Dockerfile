FROM ubuntu
MAINTAINER PMaynard

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y build-essential clang libsdl2-dev
