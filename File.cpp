#include "File.h"

File::File(){
	path = "";
}

File::File(fs::path p){
	path = p;
}

fs::path File::get_path() const{
	return path;
}

const char* File::get_filename() const{
	return path.filename().c_str();
}

double File::get_size() const {
	return fs::file_size(path);	
}
