#include <iostream>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

#define INPUT_MAX 128

class File{
	public:
		fs::path path;
		
		File();
		File(fs::path p);

		fs::path get_path() const;
		const char* get_filename() const;
		double get_size() const;
};